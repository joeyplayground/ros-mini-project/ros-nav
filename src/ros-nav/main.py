import rospy
import pika
import json
from navigation import Navigation

class MqController(object):
    def __init__(self, username, password, host='0.0.0.0', port=5672, vhost='/'):
        self._navigation = Navigation(username, password, host, port, vhost)
        self._username = username
        self._passwrd = password
        self._host = host
        self._port = port
        self._vhost = vhost
        self._connect()

    def _connect(self):
        credentials = pika.PlainCredentials(self._username, self._passwrd)
        self._connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=self._host,
                                    port=self._port,
                                    virtual_host=self._vhost,
                                    credentials=credentials))
        self._channel = self._connection.channel()
        rospy.loginfo('connected ...')

    def start(self):
        self._channel.basic_qos(prefetch_count=1)
        self._channel.basic_consume(
            queue='test_queue', on_message_callback=self._callback)
        rospy.loginfo('consuming ...')
        self._channel.start_consuming()

    def _parse_body(self, data):
        data = json.loads(data)
        if data['command']=='navigation':
            return data['command'], data['body']['action'], data['body']['target']
        return data['command'], data['body'], ''

    def _callback(self, ch, method, properties, body):
        self._channel.basic_ack(delivery_tag=method.delivery_tag)
        command, action, target = self._parse_body(body)
        if command=='navigation':
            if action==1: self._navigation.goto_target(target)
            else: self._navigation.stop()


rospy.init_node('ros_nav', anonymous=True)
mq = MqController('', '', '')
mq.start()