import rospy
import pika
import json
from yocs_msgs.msg import NavigationControl, NavigationControlStatus

class Navigation(object):
    def __init__(self, username, password, host, port, vhost):
        self._pub_nav_ctrl = rospy.Publisher('/nav_ctrl', NavigationControl, queue_size=1, latch=True)
        self._sub_nav_status = rospy.Subscriber('/nav_ctrl_status', NavigationControlStatus, self.send_signal)
        self._username = username
        self._passwrd = password
        self._host = host
        self._port = port
        self._vhost = vhost

    def goto_target(self, waypoint):
        self._pub_nav_ctrl.publish(NavigationControl(NavigationControl.START, waypoint))

    def stop(self):
        self._pub_nav_ctrl.publish(NavigationControl(NavigationControl.STOP, ''))

    def _connect(self):
        credentials = pika.PlainCredentials(self._username, self._passwrd)
        self._connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=self._host,
                                    port=self._port,
                                    virtual_host=self._vhost,
                                    credentials=credentials))
        self._channel = self._connection.channel()

    def _disconnect(self):
        self._connection.close()

    def send_signal(self, data):
        self._connect()
        msg = {'command': 'signal', 'body': data.status}
        self._channel.basic_publish(exchange='', routing_key='test_queue', body=json.dumps(msg))
        self._disconnect()
